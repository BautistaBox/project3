/*
 * File-related system call implementations.
 */

#include <types.h>
#include <kern/errno.h>
#include <kern/fcntl.h>
#include <kern/limits.h>
#include <kern/seek.h>
#include <kern/stat.h>
#include <lib.h>
#include <uio.h>
#include <proc.h>
#include <current.h>
#include <synch.h>
#include <copyinout.h>
#include <vfs.h>
#include <vnode.h>
#include <openfile.h>
#include <filetable.h>
#include <syscall.h>

/*
 * open() - get the path with copyinstr, then use openfile_open and
 * filetable_place to do the real work.
 */
int
sys_open(const_userptr_t upath, int flags, mode_t mode, int *retval)
{
	const int allflags = O_ACCMODE | O_CREAT | O_EXCL | O_TRUNC | O_APPEND | O_NOCTTY;

	char *kpath;
	struct openfile *file;
	int result = 0;
	//	size_t length;
	
	if((flags&allflags)!=flags){
	  *retval = EINVAL;
	  return -1;
	}

	//	if(upath == NULL){
	// *retval = EFAULT;
	//return -1;
	//}

	
	kpath = kmalloc(PATH_MAX);


	copyinstr((const_userptr_t)upath, kpath, PATH_MAX, NULL);
	//	if(result){
	  // *retval = EFAULT;
	//  return result;
	//	}

	result = openfile_open(kpath, flags, mode, &file);
	if(result){
	  kfree(kpath);
	  return result;
	}
	
	filetable_place(curproc->p_filetable, file, retval);

	
	

	
	/* 
	 * Your implementation of system call open starts here.  
	 *
	 * Check the design document design/filesyscall.txt for the steps
	 */
	//	(void) upath; // suppress compilation warning until code gets written
	//	(void) flags; // suppress compilation warning until code gets written
	//	(void) mode; // suppress compilation warning until code gets written
	//	(void) retval; // suppress compilation warning until code gets written
	//	(void) allflags; // suppress compilation warning until code gets written
	//	(void) kpath; // suppress compilation warning until code gets written
	//	(void) file; // suppress compilation warning until code gets written



	
	return 0;
}

/*
 * read() - read data from a file
 */
int
sys_read(int fd, userptr_t buf, size_t size, int *retval)
{
  
  int result = 0;
  struct openfile * file;
  // struct filetable table;
  struct uio userIO;
  *retval = 0;

  
  // FD number to oFile object
  filetable_get(curproc->p_filetable, fd, &file);
  
  // lock the seek position (only for seekable objects)
  lock_acquire(file->of_offsetlock);
  
  //check for files opened write only
  
  
  
  // constructing a uio here
  struct iovec iov;
  off_t offset = file->of_offset;
  uio_kinit(&iov, &userIO, buf,
	    size, offset, UIO_READ);

  //  iov->iov_ubase = buf;
  // iov->iov_len = size;
  // userIO->uio_iov = iov;
  //userIO->uio_iovcnt = 1;
  //userIO->uio_offset = file->of_offset;
  //userIO->uio_resid = size;
  //userIO->uio_segflg = UIO_USERSPACE;
  //userIO->uio_rw = rw;
  //userIO->uio_space = curthread->p_addrspace;
  
  
  // VOP_READ
  result = VOP_READ(file->of_vnode, &userIO);
  
  //if unsuccessful
  if(result){
    lock_release(file->of_offsetlock);//release open file lock
  }
  
  // amount read
  openfile_incref(file);
  
  // move seek position
  file->of_offset += size;
  
  //release lock
  lock_release(file->of_offsetlock);
  filetable_put(curproc->p_filetable, fd, file);
  
  
  /* 
   * Your implementation of system call read starts here.  
   *
   * Check the design document design/filesyscall.txt for the steps
   */
  //       (void) fd; // suppress compilation warning until code gets written
  //      (void) buf; // suppress compilation warning until code gets written
  //       (void) size; // suppress compilation warning until code gets written
  //       (void) retval; // suppress compilation warning until code gets written
  
  
  
  
  return result;
}

/*
 * write() - write data to a file
 */
int
sys_write(int fd, userptr_t buf, size_t size, int *retval)
{
  int result = 0;
  struct openfile * file;
  // struct filetable table;
  struct uio userIO;
  *retval = 0;
  // FD number to oFile object
  filetable_get(curproc->p_filetable, fd, &file);
  
  // lock the seek position (only for seekable objects)
  lock_acquire(file->of_offsetlock);
  
  //check for files opened write only

  
  // constructing a uio here
  struct iovec iov;
  off_t offset = file->of_offset;
  uio_kinit(&iov, &userIO, buf, size, offset, UIO_WRITE);
 
  // VOP_WRITE
  result = VOP_WRITE(file->of_vnode, &userIO);
  
  //if unsuccessful
  if(result){
    lock_release(file->of_offsetlock);//release open file lock
  }
  
  // amount written
  openfile_incref(file);
  
  // move seek position
  file->of_offset += size;
  
  //release lock
  lock_release(file->of_offsetlock);
  filetable_put(curproc->p_filetable, fd, file);
  
  return result;
}
/*
 * close() - remove from the file table.
 */

int
sys_close(int fd){
  
  if(!filetable_okfd(curproc->p_filetable, fd)){
    return EBADF;
  }
  struct openfile* ofile;
  filetable_placeat(curproc->p_filetable,
		    NULL,
		    fd,
		    &ofile);
  
  if(ofile==NULL)
    return EBADF;

  openfile_decref(ofile);

  return 0;
         
}


/* 
* meld () - combine the content of two files word by word into a new file
*/

int
sys_meld(const_userptr_t pn1, const_userptr_t pn2,
	 const_userptr_t pn3){
  (void) pn1;
  (void) pn2;
  (void) pn3;
  
  return 0;
}

